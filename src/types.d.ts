declare module 'clipp';

type Url = string;
type AbsoluteFilename = string;
type RelativeFilename = string;
type Filename = Url | AbsoluteFilename | RelativeFilename;

type Test = {
  name: string;
  sourceCode: string;
  startLine: number;
};

type Suite = {
  name: string;
  suites: Suite[];
  tests: Test[];
  origin: Filename;
  startLine: number;
}

type Stats = {
  counts: {
    tests: number;
    suites: number;
  }
}
