import {inspectFile} from './inspectFile.js';
import {stats} from './stats.js';

export {
    inspectFile,
    stats
};
