import * as assert from 'assert';
import {describe, it} from 'mocha';
import {stats} from './stats.js';
import {emptySuite} from './Suite.js';
import {emptyTest} from "./Test.js";

describe('Provide statistics about test suites', () => {
  it('GIVEN no test suites and no tests THEN return 0 for everything', () => {
    const noSuites = emptySuite();
    assert.deepEqual(stats(noSuites), {counts: {tests: 0, suites: 0}});
  });
  it('GIVEN one test suite with no tests THEN return the counts: suites=1, tests=0', () => {
    const suite = {...emptySuite(), name: 'test suite'};
    const suites = {...emptySuite(), suites: [suite]};
    assert.deepEqual(stats(suites), {counts: {tests: 0, suites: 1}});
  });
  it('GIVEN one test suite containing another one THEN return the counts: suites=2, tests=0', () => {
    const suite = {
      ...emptySuite(),
      name: 'suite',
      suites: [{...emptySuite(), name: 'test suite'}],
    };
    const suites = {...emptySuite(), suites: [suite]};
    assert.deepEqual(stats(suites), {counts: {tests: 0, suites: 2}});
  });
  it('GIVEN two test suites containing two each THEN return the counts: suites=6, tests=0', () => {
    const aSuite = {...emptySuite(), name: 'test suite'};
    const suite = {...emptySuite(), name: 'suite', suites: [aSuite, aSuite]};
    const suites = {...emptySuite(), suites: [suite, suite]};
    assert.deepEqual(stats(suites), {counts: {tests: 0, suites: 6}});
  });
  it('GIVEN suites multiple levels deep THEN return the right counts', () => {
    const suites = [
      emptySuite(),
      {...emptySuite(), suites: [emptySuite()]},
      {
        ...emptySuite(),
        suites: [{...emptySuite(), suites: [emptySuite()]}],
      },
      {
        ...emptySuite(),
        suites: [{
          ...emptySuite(),
          suites: [emptySuite(), emptySuite()],
        }],
      }
    ];
    assert.deepEqual(stats({...emptySuite(), suites}), {counts: {tests: 0, suites: 10}});
  });
});

describe('Provide statistics about the tests', () => {
  it('GIVEN no suites, just one test THEN return the count=1', () => {
    const oneTest = {...emptySuite(), tests: [emptyTest()]};
    assert.deepEqual(stats(oneTest), {counts: {tests: 1, suites: 0}});
  });
  it('GIVEN a test on the 2nd level of nested suites THEN return count=1', () => {
    const oneTest = {
      ...emptySuite(),
      suites: [{...emptySuite(), tests: [emptyTest()]}],
    };
    assert.deepEqual(stats(oneTest), {counts: {tests: 1, suites: 1}});
  });
  it('GIVEN tests on many levels THEN count correctly', () => {
    const test = emptyTest();
    const all = {
      ...emptySuite(),
      suites: [
        {
          ...emptySuite(),
          tests: [test, test],
        },
        {
          ...emptySuite(),
          suites: [{...emptySuite(), tests: [test]}],
          tests: [test],
        },
        {
          ...emptySuite(),
          suites: [{
            ...emptySuite(),
            suites: [{...emptySuite(), tests: [test, test]}],
            tests: [test],
          }],
          tests: [test],
        },
        {
          ...emptySuite(),
          suites: [{
            ...emptySuite(),
            suites: [
              emptySuite(),
              {...emptySuite(), tests: [test, test]}
            ],
          }],
        }
      ],
      tests: [test],
    };
    assert.deepEqual(stats(all), {counts: {tests: 11, suites: 10}});
  });
});