import ts from 'typescript';
import {createNewSuite} from './Suite.js';
import {createNewTest} from "./Test.js";

/**
 * @param {ts.FunctionExpression | ts.ArrowFunction} fn
 * @returns {string}
 */
const extractSourceCode = (fn) => {
  const functionBody = fn.body;
  if (ts.isBlock(functionBody)) {
    // The `statements.pos/end` just indicates the position in the entire source file.
    // We need to get the position relative to the function body, so we just subtract the
    // function body's position. Which basically means we get the position of the function body
    // without the curly braces around it.
    const start = functionBody.statements.pos - functionBody.pos;
    // I have no idea how to fix the comment-at-the-end problem, so I just remove the last character if it is a `}`.
    const sourceCodeMaybeEndingWithBrace = functionBody.getFullText().slice(start);
    if (sourceCodeMaybeEndingWithBrace.trimEnd().endsWith('}')) {
      return sourceCodeMaybeEndingWithBrace.slice(0, -1).trimEnd();
    }
    return sourceCodeMaybeEndingWithBrace.trimEnd();
  }
  return functionBody.getText();
};

/**
 * @param {ts.Expression} maybeFunction
 * @returns {string}
 */
const checkForAndGetSourceCode = (maybeFunction) => {
  if (false === Boolean(maybeFunction)) {
    return '';
  }
  if (ts.isArrowFunction(maybeFunction) || ts.isFunctionExpression(maybeFunction)) {
    return extractSourceCode(maybeFunction);
  }
  return '';
};

/**
 * @param {ts.SourceFile} sourceFile
 * @returns {Suite}
 */
const allSuites = (sourceFile) => {
  const suites = createNewSuite('', 0);
  /**
   * @param {ts.Node} node
   * @param {Suite} parentSuite
   */
  const searchDescendants = (node, parentSuite) => {
    const children = node.getChildren(sourceFile);
    for (const child of children) {
      if (ts.isCallExpression(child)) {
        const functionName = child.expression.getText();
        const isSuite = functionName === 'describe';
        const isTest = functionName === 'it';
        
        if (isSuite) {
          const firstArgument = child.arguments[0];
          if (ts.isStringLiteral(firstArgument)) {
            const startLine = sourceFile.getLineAndCharacterOfPosition(firstArgument.pos).line;
            const newSuite = createNewSuite(firstArgument.text, startLine);
            parentSuite.suites.push(newSuite);
            searchDescendants(child, newSuite);
          }
        } else if (isTest) {
          const firstArgument = child.arguments[0];
          if (ts.isStringLiteral(firstArgument)) {
            const startLine = sourceFile.getLineAndCharacterOfPosition(firstArgument.pos).line;
            const sourceCode = checkForAndGetSourceCode(child.arguments[1]);
            const newTest = createNewTest(firstArgument.text, sourceCode, startLine);
            parentSuite.tests.push(newTest);
          }
        }
      } else {
        searchDescendants(child, parentSuite);
      }
    }
  };
  searchDescendants(sourceFile, suites);
  return suites;
};

/**
 * @param {string} sourceCode
 * @returns {Suite}
 */
export const inspectCode = (sourceCode) => {
  const sourceFile = ts.createSourceFile(
    "fileName",
    sourceCode,
    ts.ScriptTarget.ES2020,
    true
  );
  return allSuites(sourceFile);
};
