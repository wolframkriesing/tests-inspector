export type PathNamesTree = {
    name: string
    children: PathNamesTree[], 
};

export function findRoots(files: Filename[]): string[];
export function generateSuiteTree(suites: Suite[]): Suite;
export function splitOutPathNames(files: Filename[]): string[][];
export function buildPathNamesTree(filenamesWithPath: Filename[]): PathNamesTree;
export function groupSuites(suites: Suite[]): Suite;
