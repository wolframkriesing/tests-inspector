/**
 * @returns {Suite}
 */
export const emptySuite = () => ({name: '', suites: [], tests: [], origin: '', startLine: 0});

/**
 * @param {Suite["name"]} name
 * @param {Suite["startLine"]} startLine
 * @returns {Suite}
 */
export const createNewSuite = (name, startLine) => {
  const suite = emptySuite();
  suite.name = name;
  suite.startLine = startLine;
  return suite;
};

/**
 * @param {Suite} suite
 * @returns {Suite}
 */
const cloneSuite = (suite) => {
  const clone = {...suite};
  return clone;
};

/**
 * @param {Suite} suite
 * @returns {Suite}
 */
export const cloneSuiteAndNameIt = (suite) => {
  const clone = cloneSuite(suite);
  clone.name = suite.origin;
  return clone;
}
