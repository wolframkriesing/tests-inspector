import * as assert from 'assert';
import {it, describe} from 'mocha';
import {inspectCode} from './inspectCode.js';
import {emptySuite} from './Suite.js';
import {assertThat} from "hamjest";

describe('Create test suites and tests in a tree-like structure', () => {
  describe('GIVEN the source code (as a string)', () => {
    it('WHEN it is empty THEN return no test suites', () => {
      assert.deepStrictEqual(inspectCode(''), emptySuite());
    });
    it('WHEN it contains not test THEN return no test suites', () => {
      assert.deepStrictEqual(inspectCode('var x = 1; // but no test'), emptySuite());
    });
    describe('WHEN it contains one `describe`', () => {
      it('THEN return one test suite', () => {
        assert.strictEqual(inspectCode('describe("")').suites.length, 1);
      });
      it('THEN return the test suite`s name', () => {
        const suites = inspectCode('describe("test suite")').suites;
        assert.strictEqual(suites[0].name, 'test suite');
      });
      describe('AND one `it()` inside', () => {
        const suites = () => inspectCode(`
          describe("test suite", () => {
            it('test 1', () => {});
          })`);
        it('THEN return one test', () => {
          assert.strictEqual(suites().suites[0].tests.length, 1);
        });
        it('THEN return the test`s name', () => {
          assert.strictEqual(suites().suites[0].tests[0].name, 'test 1');
        });
      });
      describe('AND many `it()`s inside', () => {
        const suites = () => inspectCode(`
          describe("test suite", () => {
            it('test 1', () => {});
            it('test 2', () => {});
            it('test 3', () => {});
          })`);
        it('THEN return many tests', () => {
          assert.strictEqual(suites().suites[0].tests.length, 3);
        });
        it('THEN return the tests` names', () => {
          assert.strictEqual(suites().suites[0].tests[0].name, 'test 1');
          assert.strictEqual(suites().suites[0].tests[1].name, 'test 2');
          assert.strictEqual(suites().suites[0].tests[2].name, 'test 3');
        });
      });
    });
    describe('WHEN it contains multiple (not nested) `describe`s', () => {
      it('THEN return all test suite`s names', () => {
        const sourceCode = `
          describe("test suite 1");
          describe("test suite 2");
          describe("test suite 3");
        `;
        const suites = inspectCode(sourceCode).suites;
        assert.strictEqual(suites[0].name, 'test suite 1');
        assert.strictEqual(suites[1].name, 'test suite 2');
        assert.strictEqual(suites[2].name, 'test suite 3');
      });
      it('THEN return ONLY test suites, no other', () => {
        const sourceCode = `
          describe("test suite 1");
          not_describe("test suite 2");
        `;
        const suites = inspectCode(sourceCode).suites;
        assert.strictEqual(suites.length, 1);
      });
    });
    describe('WHEN it contains multiple nested `describe`s', () => {
      it('one level deep THEN return all test suite`s names in a tree-like structure', () => {
        const sourceCode = `
          describe("test suite 1", () => {
            describe("test suite 1.1");
            describe("test suite 1.2");
          });
        `;
        const suites = inspectCode(sourceCode).suites;
        assert.strictEqual(suites[0].name, 'test suite 1');
        assert.strictEqual(suites[0].suites[0].name, 'test suite 1.1');
        assert.strictEqual(suites[0].suites[1].name, 'test suite 1.2');
      });
      it('many levels deep THEN return all test suite`s names in a tree-like structure', () => {
        const sourceCode = `
          describe("test suite 1", () => {
            describe("test suite 2", () => {
              describe("test suite 3", () => {
                describe("test suite 4", () => {});
              });
            });
          });
        `;
        const suites = inspectCode(sourceCode).suites;
        assert.strictEqual(suites[0].name, 'test suite 1');
        assert.strictEqual(suites[0].suites[0].name, 'test suite 2');
        assert.strictEqual(suites[0].suites[0].suites[0].name, 'test suite 3');
        assert.strictEqual(suites[0].suites[0].suites[0].suites[0].name, 'test suite 4');
      });
      it('multiple suites on many levels THEN return all test suite`s names in a tree-like structure', () => {
        const sourceCode = `
          describe("test suite 1", () => {
            describe("test suite 1.1", () => {});
            describe("test suite 1.2", () => {});
            describe("test suite 2", () => {
              describe("test suite 2.1", () => {});
              describe("test suite 2.2", () => {});
              describe("test suite 3", () => {});
            });
          });
        `;
        const suites = inspectCode(sourceCode).suites;
        assert.strictEqual(suites[0].name, 'test suite 1');
        assert.strictEqual(suites[0].suites[0].name, 'test suite 1.1');
        assert.strictEqual(suites[0].suites[1].name, 'test suite 1.2');
        assert.strictEqual(suites[0].suites[2].name, 'test suite 2');
        assert.strictEqual(suites[0].suites[2].suites[0].name, 'test suite 2.1');
        assert.strictEqual(suites[0].suites[2].suites[1].name, 'test suite 2.2');
        assert.strictEqual(suites[0].suites[2].suites[2].name, 'test suite 3');
      });
    });
  });
});

describe('Provide the tests source code', () => {
  describe('GIVEN the second param (the test function) is invalid', () => {
    it('WHEN there is no test function THEN provide empty source code', () => {
      const suites = () => inspectCode(`
        describe("test suite", () => {
          it('test 1'});
        })`);
      assertThat(suites().suites[0].tests[0].sourceCode, '');
    });
    it('WHEN it is not a function THEN provide empty source code', () => {
      const suites = () => inspectCode(`
        describe("test suite", () => {
          it('test 1', null});
        })`);
      assertThat(suites().suites[0].tests[0].sourceCode, '');
    });
  });

  describe('GIVEN the test function is an arrow function', () => {
    it('WHEN the function body is empty THEN return an empty string (as source code)', () => {
      const suites = () => inspectCode(`
        describe("test suite", () => {
          it('test 1', () => {});
        })`);
      assertThat(suites().suites[0].tests[0].sourceCode, '');
    });
    it('WHEN there is one test suite with one test (an arrow function) THEN provide the tests source code', () => {
      const suites = () => inspectCode(`
        describe("test suite", () => {
          it('test 1', () => {
            const a;
            assert.strictEqual(1, 1);
          });
        })`);
  
      const expected = `
            const a;
            assert.strictEqual(1, 1);`;
      assertThat(suites().suites[0].tests[0].sourceCode, expected);
    });
    it('WHEN the function body is not enclosed in {} THEN provide it`s source code too', () => {
      const suites = () => inspectCode(`
        describe("test suite", () => {
          it('test 1', () => a ? b: c);
        })`);
      assertThat(suites().suites[0].tests[0].sourceCode, 'a ? b: c');
    });
    it('WHEN the function body ends with a comment THEN provide the comment too', () => {
      const suites = () => inspectCode(`
        describe("test suite", () => {
          it('test 1', () => {
            a ? b: c; // end comment
          });
        })`);
      const expected = `
            a ? b: c; // end comment`;
      assertThat(suites().suites[0].tests[0].sourceCode, expected);
    });
  });
  
  describe('GIVEN the test function is an (old style) function expression', () => {
    it('WHEN the test function is a "normal" function-expression THEN provide it`s source code', () => {
      const suites = () => inspectCode(`
        describe("test suite", () => {
          it('test 1', function() {noCode();}});
        })`);
      assertThat(suites().suites[0].tests[0].sourceCode, 'noCode();');
    });
    it('WHEN the function body is empty (no matter spaces) THEN return an empty string', () => {
      const suites = () => inspectCode(`
        describe("test suite", () => {
          it('test 1', function() {      });
        })`);
      assertThat(suites().suites[0].tests[0].sourceCode, '');
    });
    it('WHEN there are no spaces in `function(){}` THEN get the right source code too (somehow this failed) #regressionTest', () => {
      const suites = () => inspectCode(`it('',function(){noCode()})`);
      assertThat(suites().tests[0].sourceCode, 'noCode()');
    });
  });
});

describe('Provide the startLine prop for tests and suites', () => {
  it('WHEN there is only one line THEN startLine=0', () => {
    const suites = () => inspectCode(`describe("test suite", () => {})`);
    assertThat(suites().suites[0].startLine, 0);
  });
  it('WHEN there are multiple suites on various lines THEN each has the right startLine prop', () => {
    const suites = () => inspectCode(`describe("test suite 1", () => {})
    
    describe("test suite 2", () => {}); describe("test suite3", () => {});
    describe("test suite 4", () => {})
    `);
    const rootSuite = suites();
    assertThat(rootSuite.suites[0].startLine, 0);
    assertThat(rootSuite.suites[1].startLine, 2);
    assertThat(rootSuite.suites[2].startLine, 2);
    assertThat(rootSuite.suites[3].startLine, 3);
  });
  it('WHEN there are multiple tests on various lines THEN each has the right startLine prop', () => {
    const suites = () => inspectCode(`describe("test suite 1", () => { it('test1'); })
    describe("test suite 2", () => { it('test2.1');  it('test2.2');
      it('test2.3');  
    });
    `);
    const rootSuite = suites();
    assertThat(rootSuite.suites[0].tests[0].startLine, 0);
    assertThat(rootSuite.suites[1].tests[0].startLine, 1);
    assertThat(rootSuite.suites[1].tests[1].startLine, 1);
    assertThat(rootSuite.suites[1].tests[2].startLine, 2);
  });
});
