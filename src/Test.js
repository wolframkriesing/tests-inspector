/**
 * @returns {Test}
 */
export const emptyTest = () => ({name: '', sourceCode: '', startLine: 0});

/**
 * @param {Test["name"]} name
 * @param {Test["sourceCode"]} sourceCode
 * @param {Test["startLine"]} startLine
 * @returns {Test}
 */
export const createNewTest = (name, sourceCode, startLine) => ({name, sourceCode, startLine});
