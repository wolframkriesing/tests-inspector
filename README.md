# Tests Inspector

This project is a tool to inspect tests.
It:
- reads a set of test files
- provides statistics, like the numbers of tests and test suites
- provides all the test descriptions in a hierarchical way
- provides the code for each test

**This project lets you read (and analyze) the tests, to understand what the code does.**  
Test descriptions are the human-readable and intended to describe what the test tests
therefore what the code does, see Examples section below.

## Use this project

### Install as npm dependency
You want to install this project as a npm dependency, you can use this:
- `npm install git+https://codeberg.org/wolframkriesing/tests-inspector.git" --save-dev`

### Inspect test files
Inspect test files:
- `git clone <this repo>`
- `cd <this repo>` and `npm install`
- `npm run inspect src/*.test.js`
  show all test descriptions of a local test file
- `npm run inspect src/*.test.js -- --json`
  show them as JSON, watch out to add the `--` before the parameter so it gets passed on to the node script
- `npm run inspect https://katas.tddbin.com/katas/es1/language/global-api/parseInt.js https://katas.tddbin.com/katas/es1/language/array-api/sort-basics.js` show the tests from remote files (here two katas as used on [jskatas.org](https://jskatas.org))

## Examples
Given a test file like the following

```javascript
describe('Extract the text from tests', () => {
  describe('GIVEN a string', () => {
    it('WHEN it is empty THEN return no test suites', () => {
      assert.deepStrictEqual(inspectCode(''), []);
    });
    it('WHEN it contains not test THEN return no test suites', () => {
      assert.deepStrictEqual(inspectCode('var x = 1; // but no test'), []);
    });
    describe('WHEN it contains one `describe`', () => {
      it('THEN return one test suite', () => {
        assert.strictEqual(inspectCode('describe("")').length, 1);
      });
      it('THEN return the test suite`s name', () => {
        const suites = inspectCode('describe("test suite")');
        assert.strictEqual(suites[0].name, 'test suite');
      });
    });
  });
});
```
When you run `npm run inspect <the-filename-of-the-file-above>`, you will get this on the
command line:
```text
Extract the text from tests
  GIVEN a string
    WHEN it is empty THEN return no test suites
    WHEN it contains not test THEN return no test suites
    WHEN it contains one `describe`
      THEN return one test suite
      THEN return the test suite`s name

Statistics
-----------
Number of suites: 3
Number of tests : 4
```

To get the result as JSON do `npm run parse-files src/stats.test.js -- --json` and you will get this on the
command line:
```json
{"suites":[{"name":"src","suites":[{"name":"src/stats.test.js","suites":[{"name":"Provide statistics about test suites","suites":[],"tests":[{"name":"GIVEN no test suites and no tests THEN return 0 for everything"},{"name":"GIVEN one test suite with no tests THEN return the counts: suites=1, tests=0"}],"origin":""}],"tests":[],"origin":"src/stats.test.js"}],"tests":[],"origin":"src"}],"stats":{"counts":{"tests":2,"suites":3}}}
```

## Why?
I believe tests are not just for validating the code one writes, I believe **tests
are the driver for the code one writes**. That means, writing a test has been preceeded
by thinking what one wants. The tests are only the result of that thought process,
and become a structured mean that reflects the use cases and the responsibilities 
that the code shall fulfill. Therefore, I strongly believe (and try to practice) 
writing tests that state what I expect the code to do. That means a colleague (and not
just another developer) should be able to read and make sense of my tests.
That's why the real domain language and not the tech language shall be used in tests.

**__Tests Inspector__ lets you read (and analyze) the tests, especially the descriptions, to understand what the code does.**

## Develop and run
This project requires only docker to run. Every excutable can be prefixed simply by `./run.sh`
which runs the command inside the docker container.
You can also run it when nodejs is installed on your machine, just leave out the prefix `./run.sh`.

You want to work on this code, you can do the following:
- `git clone <this repo>`
- if you like to work in docker do this:
  - install docker
  - `docker-compose up -d` setup and start the docker container with nodejs
  - `docker-compose exec node bash` enter the container to use the execution environment
  - if you feel like you can also try bun.sh like so `docker-compose exec bun bash` (it might not be actively maintained)

docker-compose mounts the project as a volume inside a docker container, that means only those 
files are visible and therefore just findable!

Getting started:
- build and enter the docker container as described above
- `npm install` - will install all nodejs dependencies this project needs to run
- `npm test` - run the tests
- `npm run parse-files src/inspectCode.test.js` - print the test descriptions
  of the given file
  
Development:
- `docker-compose up -d` starts the docker container using docker-compose
- `docker-compose exec node bash` opens a bash terminal running inside the container, 
  from there I can run any npm command, etc. as if it was local 
  (just that ONLY the project files are mounted into this container under `/app`)  

NPM commands (to be executed inside the docker container):
- for development most npm scripts can be prefixed with `dev:` (e.g. `npm run test` and `npm run dev:test`) 
  to be run as (I found it) most convinient while in development mode    
- `npm run dev:test` runs the tests in watch mode
- `npm run dev:typecheck` runs the typechecker in watch mode
- `npm run test` runs the tests once
- `npm run typecheck` runs the typechecker once
- `npm install` to install all the dependencies
- `npm run inspect <path/to/testfile.js>` 
  to inspect the tests in the given file and write it to stdout
  
## How I develop
- I have two parallel terminals open, one with the test watcher and another one with the typechecker in watch mode
  if I need, I open a third terminal to play around with other npm commands, like installing another dependency or
  trying out the `npm run inspect` command.
  - in the first terminal I run `npm run dev:test`
  - in the second terminal I run `npm run dev:typecheck`
