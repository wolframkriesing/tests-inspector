import hamjest from 'hamjest';
import {describe, it} from 'mocha';
import {toMarkdownFromFile} from './to-markdown.js';
const {assertThat, startsWith, equalTo} = hamjest;

const expected = `Given you add multiple response interceptors
and when the response was fulfilled
- then each interceptor is executed
- then they are executed in the order they were added
- then only the last interceptor's result is returned
- then every interceptor receives the result of it's predecessor
- and when the fulfillment-interceptor throws
    - then the following fulfillment-interceptor is not called
    - then the following rejection-interceptor is called
    - once caught, another following fulfill-interceptor is called again (just like in a promise chain)`;

describe('Convert axios` interceptors tests to readable markdown', () => {
  describe('WHEN converting ', () => {
    const convertedLine = async (lineNumber) => {
      const text = await toMarkdownFromFile('examples/axios-pr/interceptors.spec.js');
      return text.split('\n')[lineNumber];
    };
    it('THEN it starts with a capitalized "Given"', async () => {
      assertThat(await convertedLine(0), startsWith('Given'));
    });
    it('THEN the first line is the first `describe` text, capitalized', async () => {
      assertThat(await convertedLine(0), startsWith('Given you add multiple response interceptors'));
    });
    it('THEN the second line is the next `describe`', async () => {
      assertThat(await convertedLine(1), equalTo('and when the response was fulfilled'));
    });

    it('THEN adds all the tests as markdown list items', async () => {
      const expected = [
        '- then each interceptor is executed',
        '- then they are executed in the order they were added',
        '- then only the last interceptor\'s result is returned',
        '- then every interceptor receives the result of it\'s predecessor',
      ];
      assertThat(await convertedLine(2), equalTo(expected[0]));
      assertThat(await convertedLine(3), equalTo(expected[1]));
      assertThat(await convertedLine(4), equalTo(expected[2]));
      assertThat(await convertedLine(5), equalTo(expected[3]));
    });

    it('THEN add the next `describe` as markdown list item', async () => {
      assertThat(
        await convertedLine(6),
        equalTo('- and when the fulfillment-interceptor throws')
      );
    });
    it('THEN adds all the following tests', async () => {
      const expected = [
        '    - then the following fulfillment-interceptor is not called',
        '    - then the following rejection-interceptor is called',
        '    - once caught, another following fulfill-interceptor is called again (just like in a promise chain)',
      ];
      assertThat(await convertedLine(7), equalTo(expected[0]));
      assertThat(await convertedLine(8), equalTo(expected[1]));
      assertThat(await convertedLine(9), equalTo(expected[2]));
    });

    it('THEN converts the entire text right', async () => {
      const text = await toMarkdownFromFile('examples/axios-pr/interceptors.spec.js');
      assertThat(text, equalTo(expected));
    });
  });
});
