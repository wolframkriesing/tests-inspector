# Axios Interceptors Tests

I used [axios] in a project and while I was reading the docs to
learn how multiple interceptors work, I had to dig all the way into the
tests and into the source code to finally understand it fully.

This led me to improve the tests to document how axios handles
multiple interceptors (as they call them).
I refactored the tests and added some, that imho make it more explicit
how it works. See [my PR](https://github.com/axios/axios/pull/3564).

In order to update the docs, which was my initial goal, I use tests-inspector,
this tool here, to pull out the descriptions and massage them a little, so they fit well into
the docs.
